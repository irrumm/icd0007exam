<?php

require_once __DIR__ . '/../vendor/common.php';

class Ex4Tests extends UnitTestCaseExtended {

    function categoryObjectCountIsCorrect() {
        $categoryList = [];

        require 'ex4.php';

        $this->assertEqual(sizeof($categoryList), 3);
    }

    function eachCategoryContainsCorrectProducts() {

        $categoryList = [];

        require 'ex4.php';

        $cat4Products = $this->getProductNumbersByCategoryName($categoryList, 'cat4');
        $cat5Products = $this->getProductNumbersByCategoryName($categoryList, 'cat5');
        $cat6Products = $this->getProductNumbersByCategoryName($categoryList, 'cat6');

        $this->assertEqual($cat4Products, ['p3', 'p6', 'p7']);
        $this->assertEqual($cat5Products, ['p1', 'p2', 'p5']);
        $this->assertEqual($cat6Products, ['p4', 'p8']);
    }

    private function getProductNumbersByCategoryName($categoryList, $categoryName) : array {
        foreach ($categoryList as $category) {
            if ($category->name !== $categoryName) {
                continue;
            }

            $productNumbers = array_map(function ($each) {
                return $each->number;
            }, $category->getProducts());

            sort($productNumbers);

            return $productNumbers;
        }

        throw new RuntimeException('did not find category: ' . $categoryName);
    }


}

(new Ex4Tests())->run(new PointsReporter(
    [1 => 1,
     2 => 10]));

