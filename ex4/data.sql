CREATE TABLE products (id INTEGER PRIMARY KEY,
                       product_number VARCHAR(255),
                       category_id VARCHAR(255));

CREATE TABLE categories (id INTEGER PRIMARY KEY,
                         name VARCHAR(255));

INSERT INTO products VALUES (1, 'p1', 1);
INSERT INTO products VALUES (2, 'p2', 1);
INSERT INTO products VALUES (3, 'p3', 3);
INSERT INTO products VALUES (4, 'p4', 5);
INSERT INTO products VALUES (5, 'p5', 1);
INSERT INTO products VALUES (6, 'p6', 3);
INSERT INTO products VALUES (7, 'p7', 3);
INSERT INTO products VALUES (8, 'p8', 5);

INSERT INTO categories VALUES (1, 'cat5');
INSERT INTO categories VALUES (2, 'cat1');
INSERT INTO categories VALUES (3, 'cat4');
INSERT INTO categories VALUES (4, 'cat3');
INSERT INTO categories VALUES (5, 'cat6');
